# Rust 标准库中文版


这是翻译 [Rust 库](https://github.com/rust-lang/rust/tree/master/library) 的地方， 相关源代码来自于 <https://github.com/rust-lang/rust>。

如果您不会说英语，那么拥有使用中文的文档至关重要，即使您会说英语，使用母语也仍然能让您感到愉快。Rust 标准库是高质量的，内容丰富严谨且非常权威，是深入学习 Rust 的第一手资料，不管是新手还是老手，都可以从中受益。市面上有很多的 Rust 教程，然而，想要成为 Rust 高手，那么阅读核心库和标准库的源码则是必要的手段。

该仓库包含了 `rust-src` 组件的所有源代码文件，并对其所有的源代码进行翻译。主要包括对 Rust 核心库的翻译，Rust 标准库的翻译，以及其他一些资源。该仓库使用 [`Cmtor`](#) (我写的效率工具) 程序并借助 `JSON` 文件来完成翻译的基本工作，当 Rust 更新时，将尽可能为其生成中文翻译。



## 在 IDE 中使用中文文档

请查看：[使用 Rust 中文文档](./docs/Install.md)




## 离线 HTML 中文 API 文档

请查看：[构建离线 HTML 文档](./docs/BuildHtml.md)



## Gitee 国内镜像

为方便国内同学的下载和访问，[rust-library-i18n](https://github.com/wtklbm/rust-library-i18n) 仓库已同步到了 [rust-library-chinese](https://gitee.com/wtklbm/rust-library-chinese)，但它仅为镜像仓库，如果您想找到我，请在 [Github](https://github.com/wtklbm) 上进行留言。




## 使用前的注意事项

该翻译基于机器翻译，并在此基础上，逐步对内容进行处理。受限于条件限制，该翻译的每次更新并非意味着内容都以全部校对且完整无误。要想了解详细信息，请查看 [注意事项](./docs/NOTE.md)，如果您使用了该仓库提供的内容，则表示您同意并默许注意事项中的所有内容。

之前旧版本的中文文档中可能会有一些未发现的严重问题，请保持实时更新，避免使用旧版本的中文文档。如果您使用了其他人提供的中文文档副本，那么请联系相关人员进行更新。

感谢所有为项目 [作出贡献的同学](https://github.com/wtklbm/rust-library-i18n/graphs/contributors)，和所有提供宝贵意见的同学。



## Others

### `crm`

`crm` 是一个镜像源管理工具，内置了 5 种 Rust 国内镜像源，并提供了测速功能，更换镜像源特别方便。

- [从 Github 访问](https://github.com/wtklbm/crm)
- [从 Gitee 访问](https://gitee.com/wtklbm/crm)




## License

MIT OR Apache-2.0

