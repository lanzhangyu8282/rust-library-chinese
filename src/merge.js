const { join } = require('path');
const { readdir, writeFile, mkdir } = require('fs/promises');
const { splitByStep } = require('@curong/array');

const getSentences = async path => {
    const rootDir = join(__dirname, `./${path}`);
    const relativePaths = await readdir(rootDir);

    return relativePaths.reduce(
        (memo, relativePath) =>
            memo.concat(require(join(rootDir, relativePath))),
        []
    );
};

async function main() {
    const newArr = await getSentences('新的');
    const oldMap = (await getSentences('老的')).reduce((m, v) => {
        m.set(v.source, v);
        return m;
    }, new Map());

    for (let i = 0, len = newArr.length, sentence; i < len; i++) {
        sentence = newArr[i];

        if (oldMap.has(sentence.source)) {
            newArr[i] = oldMap.get(sentence.source);
        }
    }

    const jsonData = splitByStep(newArr, 1e3);
    const addLf = value => `${value}\n`;
    const toJSON = value => JSON.stringify(value, null, '    ');
    const padStart = i => i.toString().padStart(2, '0');
    const pagePath = join(__dirname, `new`);
    const getFileName = id => join(pagePath, `page_${padStart(id)}.json`);

    let i = 0;
    let filePath;
    let fileBody;

    await mkdir(pagePath, { recursive: true }).catch(() => {});

    while (i < jsonData.length) {
        filePath = getFileName(i);
        fileBody = addLf(toJSON(jsonData[i++]));
        await writeFile(filePath, fileBody);
    }
}

main().then(console.log, console.error);
