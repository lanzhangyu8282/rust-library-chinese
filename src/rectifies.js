const { join } = require('path');
const { readdir, writeFile } = require('fs/promises');

const toJSON = value => `${JSON.stringify(value, null, '    ')}\n`;

const rectifies = [
    // {
    //     match: //gi,
    //     error: //g,
    //     to: ''
    // }
];

async function main() {
    const rootDir = join(__dirname, './sentences/zh-CN');
    const relativePaths = await readdir(rootDir);

    for (let i = 0, len = relativePaths.length; i < len; i++) {
        const filePath = join(rootDir, relativePaths[i]);
        const sentences = require(filePath);

        for (let i = 0, len = sentences.length, sentence; i < len; i++) {
            sentence = sentences[i];
            let { source, suggest } = sentence;

            rectifies.forEach(({ match, error, to }) => {
                if (match.test(source)) {
                    while (error.test(suggest)) {
                        suggest = suggest.replace(error, to);
                        error.lastIndex = 0;
                    }

                    sentence.suggest = suggest;
                    match.lastIndex = 0;
                }
            });
        }

        await writeFile(filePath, toJSON(sentences));
    }
}

main().then(console.log, console.error);
